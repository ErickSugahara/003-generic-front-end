import { combineReducers } from 'redux';
import linearScaling from './linear_scaling/linear_scaling';
import userReducer from './userReducer';

const rootReducer = combineReducers({
    linearScaling: linearScaling,
    userReducer: userReducer
});

export default rootReducer;