const INITIAL_STATE = {
    result: ''
}

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'SCALING':
            return { ...state, result: action.payload.result }
        default:
            return state
    }
}