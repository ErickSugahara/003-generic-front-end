const INITIAL_STATE = {
    list: [],
    user: {
        groups: [],
        active: true,
        _id: null,
        name: null,
        lastName: null,
        email: null,
        cpf: null,
        rg: null,
        __v: null
    }
}

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case 'USER':
            return { ...state, list: action.payload }
        default:
            return state
    }
}