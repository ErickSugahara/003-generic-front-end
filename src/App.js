import React from 'react';
import { connect } from 'react-redux'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { Layout, Menu, Icon, Input, Button } from 'antd';
import { scalingMock, postScaling } from './actions/scaling_action';
import "./assets/css/logo.css";
import Role from "./components/role/role";
import User from "./components/user/user";

const { Header, Sider, Content, Footer } = Layout;

const SubMenu = Menu.SubMenu;
//const MenuItemGroup = Menu.ItemGroup;

class App extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      collapsed: false,
      result: ''
    }
  }

  componentDidMount() {
  }

  handleTextChange(event, prop) {
    if (prop === "TERM") {
      this.setState({ terms: event.target.value })
    }

    if (prop === "MATRIX") {
      this.setState({ matrix: event.target.value })
    }
  }

  toggle() {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  linkTo = (item) => {
    console.log(item);
    //FlowRouter.go(item.key);
  };

  render() {
    //const WrappedDynamicFieldSet = Form.create({ name: 'dynamic_form_item' })(DynamicFieldSet);
    return (
      <div>
        <Router>
          <Layout>
            <Header className="header">
              <div className="logo" />
              <Menu
                theme="dark"
                mode="horizontal"
                defaultSelectedKeys={['2']}
                style={{ lineHeight: '64px' }}
              >
                {/**
            <Menu.Item key="1">nav 1</Menu.Item>
            <Menu.Item key="2">nav 2</Menu.Item>
            <Menu.Item key="3">nav 3</Menu.Item>
           */}
              </Menu>
            </Header>
            <Content style={{ padding: '0 50px' }}>
              {
                /*
                <Breadcrumb style={{ margin: '16px 0' }}>
                  <Breadcrumb.Item>Home</Breadcrumb.Item>
                  <Breadcrumb.Item>List</Breadcrumb.Item>
                  <Breadcrumb.Item>App</Breadcrumb.Item>
                </Breadcrumb>
                */
              }
              <Layout style={{ padding: '24px 0', background: '#fff' }}>
                <Sider width={200} style={{ background: '#fff' }}>
                  <Menu
                    mode="inline"
                    defaultSelectedKeys={['0']}
                    defaultOpenKeys={['sub1', 'sub2']}
                    style={{ height: 'auto' }}
                  >
                    <SubMenu key="sub1" title={<span><Icon type="user" />Admin</span>}>
                      <Menu.Item key="1">
                        <Link to="user">User</Link>
                      </Menu.Item>
                      <Menu.Item key="2">
                        <Link to="group">Group</Link>
                      </Menu.Item>
                      <Menu.Item key="3">
                        <Link to="role">Role</Link>
                      </Menu.Item>
                    </SubMenu>
                    {/*<SubMenu key="sub2" title={<span><Icon type="laptop" />API</span>}>
                    <Menu.Item key="4">Linear Scaling</Menu.Item>
          </SubMenu>*/}
                  </Menu>
                </Sider>
                <Content style={{ padding: '0 24px' }}>
                  <Route path="/user" component={User} />
                  <Route path="/role" component={Role} />
                </Content>
              </Layout>
            </Content>
            <Footer style={{ textAlign: 'center' }}>
              Mock Platform ©2019 Created by Érick Kenji Sugahara
          </Footer>
          </Layout>
        </Router>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    result: state.linearScaling.result
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ postScaling }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(App)