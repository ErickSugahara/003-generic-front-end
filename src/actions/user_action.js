import axios from 'axios';

const headers = {
    'Accept': 'application/json'
}

let url = 'http://localhost:5000/api/user'

const INITIAL_VALUE = {
    _id: null,
    name: null,
    lastName: null,
    email: null,
    cpf: null,
    rg: null,
    groups: [],
    active: true
}

export const getUserList = () => {
    return dispatch => {
        axios({
            method: 'get',
            url: url,
            headers: headers
        }).then(resp => {
            //console.log('Requisição bem sucedida:', resp)
            dispatch(
                { type: 'USER', payload: resp.data }
            )
        }).catch(error => {
            //console.log('Erro ao executar requisição:', error)
            dispatch(
                { type: 'USER', payload: { result: "Erro ao executar requisição. Verifique se a aplicação se encontra disponível." } }
            )
        })
    }
}

export const addUser = (user) => {
    return dispatch => {
        axios({
            method: 'post',
            url: url,
            headers: headers,
            data: user
        }).then(resp => {
            //console.log('Requisição bem sucedida:', resp)
            axios({
                method: 'get',
                url: url,
                headers: headers
            }).then(resp => {
                //console.log('Requisição bem sucedida:', resp)
                dispatch(
                    { type: 'USER', payload: resp.data }
                )
            }).catch(error => {
                //console.log('Erro ao executar requisição:', error)
                dispatch(
                    { type: 'USER', payload: { result: "Erro ao executar requisição. Verifique se a aplicação se encontra disponível." } }
                )
            })
        }).catch(error => {
            //console.log('Erro ao executar requisição:', error)
            dispatch(
                { type: 'USER', payload: { result: "Erro ao executar requisição. Verifique se a aplicação se encontra disponível." } }
            )
        })
    }
}

export const editUser = (user) => {
    return dispatch => {
        axios({
            method: 'put',
            url: url + "/" + user._id,
            headers: headers,
            data: user
        }).then(resp => {
            //console.log('Requisição bem sucedida:', resp)
            axios({
                method: 'get',
                url: url,
                headers: headers
            }).then(resp => {
                //console.log('Requisição bem sucedida:', resp)
                dispatch(
                    { type: 'USER', payload: resp.data }
                )
            }).catch(error => {
                //console.log('Erro ao executar requisição:', error)
                dispatch(
                    { type: 'USER', payload: { result: "Erro ao executar requisição. Verifique se a aplicação se encontra disponível." } }
                )
            })
        }).catch(error => {
            //console.log('Erro ao executar requisição:', error)
            dispatch(
                { type: 'USER', payload: { result: "Erro ao executar requisição. Verifique se a aplicação se encontra disponível." } }
            )
        })
    }
}

export const deleteUser = (id) => {
    return dispatch => {
        axios({
            method: 'delete',
            url: url + "/" + id,
            headers: headers
        }).then(resp => {
            //console.log('Requisição bem sucedida:', resp)
            axios({
                method: 'get',
                url: url,
                headers: headers
            }).then(resp => {
                //console.log('Requisição bem sucedida:', resp)
                dispatch(
                    { type: 'USER', payload: resp.data }
                )
            }).catch(error => {
                //console.log('Erro ao executar requisição:', error)
                dispatch(
                    { type: 'USER', payload: { result: "Erro ao executar requisição. Verifique se a aplicação se encontra disponível." } }
                )
            })
        }).catch(error => {
            //console.log('Erro ao executar requisição:', error)
            dispatch(
                { type: 'USER', payload: { result: "Erro ao executar requisição. Verifique se a aplicação se encontra disponível." } }
            )
        })
    }
}