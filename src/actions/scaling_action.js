import axios from 'axios';

const headers = {
    'Accept': 'application/json'
}
let url = 'http://localhost:5000/api/scaling'

export const scalingMock = () => {
    return dispatch => {
        axios({
            method: 'get',
            url: url,
            headers: headers
        }).then(resp => {
            //console.log('Requisição bem sucedida:', resp)
            dispatch(
                { type: 'SCALING', payload: resp.data }
            )
        }).catch(error => {
            //console.log('Erro ao executar requisição:', error)
            dispatch(
                { type: 'SCALING', payload: {result:"Erro ao executar requisição. Verifique se a aplicação se encontra disponível."}}
            )
        })
    }
}

export const postScaling = (matrix, terms) => {
    return dispatch => {
        axios({
            method: 'post',
            url: url,
            headers: headers,
            data: {
                matrix: matrix, 
                terms: terms
            }
        }).then(resp => {
            //console.log('Requisição bem sucedida:', resp)
            dispatch(
                { type: 'SCALING', payload: resp.data }
            )
        }).catch(error => {
            //console.log('Erro ao executar requisição:', error)
            dispatch(
                { type: 'SCALING', payload: {result:"Erro ao executar requisição. Verifique se a aplicação se encontra disponível."}}
            )
        })
    }
}