import axios from 'axios';

const headers = {
    'Accept': 'application/json'
}
let url = 'http://localhost:5000/api/role'

export const getRole = () => {
    return dispatch => {
        axios({
            method: 'get',
            url: url,
            headers: headers
        }).then(resp => {
            //console.log('Requisição bem sucedida:', resp)
            dispatch(
                { type: 'ROLE', payload: resp.data }
            )
        }).catch(error => {
            //console.log('Erro ao executar requisição:', error)
            dispatch(
                { type: 'ROLE', payload: {result:"Erro ao executar requisição. Verifique se a aplicação se encontra disponível."}}
            )
        })
    }
}

export const createRole = (matrix, terms) => {
    return dispatch => {
        axios({
            method: 'post',
            url: url,
            headers: headers,
            data: {
                matrix: matrix, 
                terms: terms
            }
        }).then(resp => {
            //console.log('Requisição bem sucedida:', resp)
            dispatch(
                { type: 'ROLE', payload: resp.data }
            )
        }).catch(error => {
            //console.log('Erro ao executar requisição:', error)
            dispatch(
                { type: 'ROLE', payload: {result:"Erro ao executar requisição. Verifique se a aplicação se encontra disponível."}}
            )
        })
    }
}