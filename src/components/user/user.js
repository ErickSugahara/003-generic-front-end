import React, { Component } from "react";
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Table, Divider, Tag, Button, Modal, Input, Row } from 'antd';
import { getUserList, addUser, editUser, deleteUser } from '../../actions/user_action';
import "../../assets/css/modal.css"

const INNITAL_USER = {
    _id: null,
    name: null,
    lastName: null,
    email: null,
    cpf: null,
    rg: null,
    groups: [],
    active: true
}

let ACTION_ADD = true;

class User extends Component {

    constructor(props) {
        super(props);

        this.state = {
            list: [],
            visible: false,
            user: Object.assign({}, INNITAL_USER)
        };

        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.props.getUserList();
    }

    showModal = () => {
        ACTION_ADD = true;
        this.setState({
            visible: true,
        });
    };

    handleOk = (e) => {
        if (ACTION_ADD) {
            this.props.addUser(this.state.user);
        } else {
            this.props.editUser(this.state.user);
        }
        this.setState({
            visible: false,
            user: Object.assign({}, INNITAL_USER)
        });
        this.props.getUserList();
    };

    handleCancel = (e) => {
        this.setState({
            visible: false,
            user: Object.assign({}, INNITAL_USER)
        });
    };

    handleChange(event) {
        this.setState({ user: Object.assign(this.state.user, { [event.target.name]: event.target.value }) });
    }

    editUser(exUser) {
        this.setState({
            user: {
                _id: exUser._id,
                name: exUser.name,
                lastName: exUser.lastName,
                email: exUser.email,
                cpf: exUser.cpf,
                rg: exUser.rg,
                groups: exUser.groups,
                active: exUser.active
            }
        })
        this.setState({ visible: true });
        ACTION_ADD = false;
    }

    render() {
        const columns = [
            {
                title: 'Nome',
                dataIndex: 'name',
                key: 'name',
                //render: text => <a href="javascript:;">{text}</a>,
            },
            {
                title: 'Sobrenome',
                dataIndex: 'lastName',
                key: 'lastName',
            },
            {
                title: 'e-mail',
                dataIndex: 'email',
                key: 'email',
            },
            {
                title: 'CPF',
                dataIndex: 'cpf',
                key: 'cpf',
            },
            {
                title: 'RG',
                dataIndex: 'rg',
                key: 'rg',
            },
            {
                title: 'Groups',
                key: 'groups',
                dataIndex: 'groups',
                render: groups => (
                    <span>
                        {groups.map(group => {
                            let color = group.length > 5 ? 'geekblue' : 'green';
                            if (group === 'loser') {
                                color = 'volcano';
                            }
                            return (
                                <Tag color={color} key={group}>
                                    {group.toUpperCase()}
                                </Tag>
                            );
                        })}
                    </span>
                ),
            },
            {
                title: 'Action',
                key: 'action',
                render: (text, record) => (
                    <span>
                        <a onClick={() => { this.editUser(record) }}>Edit</a>
                        <Divider type="vertical" />
                        <a onClick={() => { this.props.deleteUser(record._id) }}>Delete</a>
                    </span>
                ),
            },
        ];

        return (
            <div>
                <div>
                    <Table dataSource={this.props.list} rowKey="_id" columns={columns} />
                    <Button type="primary" onClick={this.showModal}>Novo</Button>
                </div>
                <div>
                    <Modal
                        title="Novo Usuário"
                        visible={this.state.visible}
                        onOk={this.handleOk}
                        onCancel={this.handleCancel}
                    >
                        <label form="inputName">Nome:</label>
                        <Input id="inputName" name="name" placeholder="Nome" value={this.state.user.name} onChange={this.handleChange} />

                        <Row className="row">
                            <label form="inputLastName">Sobrenome:</label>
                            <Input id="inputLastName" name="lastName" placeholder="Sobrenome" value={this.state.user.lastName} onChange={this.handleChange} />
                        </Row>
                        <Row className="row">
                            <label form="inputRG">RG:</label>
                            <Input id="inputRG" name="rg" placeholder="RG" value={this.state.user.rg} onChange={this.handleChange} />
                        </Row>
                        <Row className="row">
                            <label form="inputCPF">CPF:</label>
                            <Input id="inputCPF" name="cpf" placeholder="CPF" value={this.state.user.cpf} onChange={this.handleChange} />
                        </Row>
                        <Row className="row">
                            <label form="inputEmail">E-mail</label>
                            <Input id="inputEmail" name="email" placeholder="e-mail" value={this.state.user.email} onChange={this.handleChange} />
                        </Row>
                    </Modal>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        list: state.userReducer.list,
        user: state.userReducer.user
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ getUserList, addUser, editUser, deleteUser }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(User)